# نقد و بررسی سند راهبردی

## درباره مخزن
نظر به اهمیت توسعه و به‌کارگیری نرم‌افزارهای آزاد در سطح حکومتی، و نیز نظر به اهمیت جایگاه جامعهٔ نرم‌افزار آزاد در توجه به ظرافت‌ها در زمان تدوین و نظارت در زمان اجرای سیاست‌های مرتبط با این موضوع، این مخزن به منظور گردآوری نقدها و بررسی‌های علاقه‌مندان و صاحب‌نظران جامعه نرم‌افزار آزاد در خصوص سند راهبردی توسعه و به کارگیری نرم‌افزارهای آزاد/متن‌باز جمهوری اسلامی ایران ایجاد شده است.

آخرین ویرایش این سند را می‌توانید از [این‌جا](https://gitlab.com/flossir/floss/blob/master/strategicDocument-139102.pdf) دریافت کنید. مطالعه سند مربوط به [پروژه طرح مهاجرت به نرم‌افزارهای آزاد/متن‌باز](https://gitlab.com/flossir/floss/blob/master/FOSS_Migration_RPT_1_Sanad_Review_v3_900825.pdf) نیز توصیه می‌شود.

## شیوه طرح نقد 

 - پیشنهاد می‌شود پیش از بحث درباره نقد و نظر دیگران، نقد و نظر خود را به صورت مکتوب منتشر کنید
 - پیشنهاد می‌شود نقد خود را پیش از انتشار در این مخزن، در وبلاگ خود منتشر کنید
 - هر نقد، به صورت مجزا به صورت یک [مساله یا issue](https://gitlab.com/flossir/floss/-/boards) روی همین مخزن ایجاد شود. ترجیحا هم متن و هم پیوند به محل اصلی انتشار نقد (مثلا وبلاگ شخصی) درج شود.
 - بحث و گفتگو پیرامون هر نقد، ذیل همان مساله مطرح گردد.